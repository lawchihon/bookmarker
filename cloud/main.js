Parse.Cloud.define("hello", function(request, response) {
	response.success("Hello world!");
});

/* Login Function */
Parse.Cloud.define("logIn", function(request, response) {
	var email = request.params.logInEmail;
	var password = request.params.logInPassword;

	Parse.User.logIn(email, password, {
		success: function(user) {
			response.success("Login Successful!");
			return;
		},
		error: function(user, error) {
			response.error("Failed to login!");
			return;
		}
	});
});

/* Sign Up Function */
Parse.Cloud.define("signUp", function(request, response) {
	/* Fill in information for the new user */
	var user = new Parse.User();

	user.set("username", request.params.email);

	user.set("password", request.params.password);

	user.set("firstName", request.params.firstName);

	user.set("lastName", request.params.lastName);

	user.set("email", request.params.email);

	user.signUp(null, {
		success: function(user) {
			/* Response success message to check */
			response.success("Account created!");
		},
		error: function(user, error) {
			/* Show the error message somewhere and let the user try again. */
			response.error("This email has been used!");
		}
	});
});

/* Get First Name Function */
Parse.Cloud.define("getfirstName", function(request, response) {
	response.success(request.user.get("firstName"));
});